<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tentukan Nilai</title>
</head>
<body>
<?php
function tentukan_nilai($number)
{
    echo "<br>";

    echo "Nilaimu adalah: $number --- ";

    if ($number >= 85 && $number <= 100)
        return "Keren, nilaimu sangat baik sekali, wowwww";
    
    if ($number >= 70 && $number < 85)
        return "Mantap, nilaimu baik, josss";

    if ($number >= 60 && $number < 70)
        return "Okeee, kurasa nilaimu sudah cukup lahhh";

    else 
        return "Itu nilai atau apaan, kok kurang :'v";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
</body>
</html>

<?php 
require_once('animal.php');
require_once('Frog.php');
require_once('ape.php');

//release 0
$sheep = new animal ("shaun");
echo "Name : $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "Cold Blood : $sheep->cold_blooded <br>";
echo "<br><br>";

//release 1
$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name . <br>";
echo "Legs : $sungokong->legs . <br>"; // 2
echo "Cold Blood : $sungokong->cold_blooded . <br>"; // false
$sungokong->yell(); // "Auooo"
echo "<br><br>";

echo "<br>" ;

$kodok = new Frog("buduk");
echo "Name : $kodok->name . <br>";
echo "Legs : $kodok->legs . <br>"; // 4
echo "Cold Blood : $kodok->cold_blooded . <br>"; // true
$kodok->jump(); // "hop hop"
?>
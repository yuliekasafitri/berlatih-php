<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ubah ubah_huruf</title>
</head>
<body>
<?php
function ubah_huruf($string) 
{
    $output = "";

    for ($i = 0; $i < strlen($string); $i++) {
        $output[$i] = chr(ord($string[$i]) + 1);
    }

    return "$string = $output <br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
</body>
</html>